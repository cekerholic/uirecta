import React, { Component, PropTypes } from 'react';
import E from '../constants';

export class Label extends Component {
  static propTypes = {
    backgroundColor: PropTypes.string,
    className: PropTypes.string,
    styles: PropTypes.object,
  };

  static defaultProps = {
    backgroundColor: E.material.teal500,
  };

  render() {
    let labelStyle = {
      backgroundColor: this.props.backgroundColor,
      color: E.material.white,
      borderRadius: (1 / 6) + 'em',
      fontSize: '75%',
      textAlign: 'center',
      whiteSpace: 'nowrap',
      padding: '.3em .6em',
      display: 'inline-block',
      lineHeight: 1,
      fontWeight: 500
    }

    return <div className='label' style={Object.assign(labelStyle, this.props.styles)} {...this.props} />;
  }
}
