import React, { Component, PropTypes } from 'react';
import E from '../constants';

export class Wrapper extends Component {
  static propTypes = {
    styles: PropTypes.object,
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
  };

  static defaultProps = {
    className: 'clearfix'
  };

  render() {
    return (
      <div className={this.props.className} style={this.props.styles} {...this.props} />
    );
  }
}
