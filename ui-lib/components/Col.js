import React, { Component, PropTypes } from 'react';
import E from '../constants';

export class Col extends Component {
  static propTypes = {
    xs: PropTypes.string,
    sm: PropTypes.string,
    md: PropTypes.string,
    lg: PropTypes.string,
    gutter: PropTypes.number,
    styles: PropTypes.object,
    pushXs: PropTypes.string,
    pushSm: PropTypes.string,
    pushMd: PropTypes.string,
    pushLg: PropTypes.string,
    pullXs: PropTypes.string,
    pullSm: PropTypes.string,
    pullMd: PropTypes.string,
    pullLg: PropTypes.string,
  };

  static defaultProps = {
    gutter: E.width.gutter
  };

  constructor(props) {
    super(props);

    this.state = {
      windowWidth: window.innerWidth
    }
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResize)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize)
  }

  handleResize = (e) => {
    this.setState({
      windowWidth: window.innerWidth
    })
  }

  render() {
    let { gutter,
      xs, sm, md, lg,
      pushXs, pushSm, pushMd, pushLg,
      pullXs, pullSm, pullMd, pullLg } = this.props;
    let { windowWidth } = this.state;

    let columnStyle = {
      float: 'left',
      paddingLeft: (gutter / 2),
      paddingRight: (gutter / 2),
      position: 'relative',
    }

    if (windowWidth < E.breakpoint.sm) {
      columnStyle.width = xs;
      columnStyle.left = pushXs;
      columnStyle.right = pullXs;
    } else if (windowWidth < E.breakpoint.md) {
      columnStyle.width = sm || xs;
      columnStyle.left = pushSm || pushXs;
      columnStyle.right = pullSm || pullXs;
    } else if (windowWidth < E.breakpoint.lg) {
      columnStyle.width = md || sm || xs;
      columnStyle.left = pushMd || pushSm || pushXs;
      columnStyle.right = pullMd || pullSm || pullXs;
    } else {
      columnStyle.width = lg || md || sm || xs;
      columnStyle.left = pushLg || pushMd || pushSm || pushXs;
      columnStyle.right = pullLg || pullMd || pullSm || pullXs;
    }

    if (columnStyle.width in E.fractions) {
      columnStyle.width = E.fractions[columnStyle.width];
    }

    if (!columnStyle.width) {
      columnStyle.width = '100%';
    }

    if (columnStyle.right in E.fractions) {
      columnStyle.right = E.fractions[columnStyle.right];
    }
    if (columnStyle.left in E.fractions) {
      columnStyle.left = E.fractions[columnStyle.left];
    }

    return <div style={Object.assign(columnStyle, this.props.styles)} {...this.props} />;

  }
}
