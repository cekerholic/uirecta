import React, { Component, PropTypes } from 'react';
import E from '../constants';

export class Container extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    gutter: PropTypes.number,
    maxWidth: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    styles: PropTypes.object
  };

  static defaultProps = {
    gutter: E.width.gutter,
    maxWidth: E.width.container
  };

  render() {
    let { gutter, maxWidth } = this.props;
    let containerStyle = {
      paddingLeft: (gutter / 2),
      paddingRight: (gutter / 2),
      maxWidth: maxWidth
    }

    return <div
      className='container'
      style={Object.assign(containerStyle, this.props.styles)}
      {...this.props} />;
  }
}
