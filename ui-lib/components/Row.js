import React, { Component, PropTypes } from 'react';
import E from '../constants';

export class Row extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    gutter: PropTypes.number,
    styles: PropTypes.object
  };

  static defaultProps = {
    gutter: E.width.gutter
  };

  render() {
    let { gutter } = this.props;
    let containerStyle = {
      marginLeft: (gutter / -2),
      marginRight: (gutter / -2)
    }

    return <div className='row' style={Object.assign(containerStyle, this.props.styles)} {...this.props} />;
  }
}
