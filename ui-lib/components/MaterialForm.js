import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import Textarea from 'react-textarea-autosize';
import E from '../constants';

export class MaterialForm extends Component {
  static propTypes = {
    type: PropTypes.string,
    label: PropTypes.string,
    id: PropTypes.string,
    multiline: PropTypes.bool,
    disabled: PropTypes.bool,
    styles: PropTypes.object,
    color: PropTypes.string,
    focusColor: PropTypes.string,
    value: React.PropTypes.oneOfType([
      React.PropTypes.number,
      React.PropTypes.string
    ]),
    maxLength: PropTypes.number,
    hint: PropTypes.string,
    hintError: PropTypes.bool,
    hintColor: PropTypes.string,
  };

  static defaultProps = {
    type: 'text',
    color: E.material.grey400,
    focusColor: E.material.teal500,
    hintColor: E.material.teal500,
    value: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      styles: this.props.styles,
      value: this.props.value
    }
  }

  focusHandler = (e) => {
    this.setState({
      styles: {
        borderBottomColor: this.props.focusColor,
        boxShadow: '0 1px 0 ' + this.props.focusColor,
        maxHeight: 'none',
      },
      labelStyle: {
        color: this.props.focusColor,
        fontSize: '1.2rem',
        transform: 'translateY(-2rem)',
      }
    })
  }

  changeHandler = (e) => {
    this.setState({
      value: e.target.value
    })
  }

  blurHandler = (e) => {
    if (this.state.value === null || this.state.value === '') {

      this.setState({
        styles: {
          borderBottomColor: this.props.color
        },
        labelStyle: {
          color: this.props.color,
          fontSize: '1.6rem',
          transform: 'translateY(0)'
        }
      })
    }
  }

  render() {
    let fieldStyle = {
      borderBottomColor: this.props.color,
      maxHeight: 45
    }

    if (this.props.disabled) {
      fieldStyle = {
        borderBottomStyle: 'dashed',
        color: this.props.color,
      }
    }

    let labelStyle = {
      color: this.props.color,
    }

    let inputType = 'text';

    let inputLabel = this.props.label ? (
      <label
        className="material-form__label"
        htmlFor={this.props.id}
        style={Object.assign(labelStyle, this.state.labelStyle)}
      >
        {this.props.label}
      </label>
    ) : null;

    let hintClass = classNames(
      'material-form__hint',
      (this.props.hintError ? 'is-error' : 'is-success')
    );

    let hintStyle = {
      color: this.props.hintColor,
    }

    if(this.props.hintError) {
      hintStyle = {
        color: E.material.red500,
      }
    }

    let hintText = this.props.hint ? (
      <div
        className={hintClass}
        style={hintStyle}>
        {this.props.hint}
      </div>
    ) : null;


    let TextField = 'input';

    if (this.props.multiline) {
      TextField = Textarea;
    }

    return (
      <div className='material-form'>
        {inputLabel}
        <TextField
          className="material-form__field"
          style={Object.assign(fieldStyle, this.state.styles)}
          onFocus={this.focusHandler}
          onChange={this.changeHandler}
          onBlur={this.blurHandler}
          {...this.props}
          label={null} />
        {hintText}
      </div>
    );
  }
}
