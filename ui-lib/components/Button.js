import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import Ink from 'react-ink';
import E from '../constants';

export class Button extends Component {
  static propTypes = {
    size: PropTypes.oneOf(['xs', 'md', 'sm', 'lg']),
    backgroundColor: PropTypes.string,
    block: PropTypes.bool,
    radii: PropTypes.number,
    styles: PropTypes.object,
    href: PropTypes.string,
    anchor: PropTypes.bool,
    type: PropTypes.string,
    value: PropTypes.string,
    shade: PropTypes.oneOf(['light', 'dark']),
    hover: PropTypes.bool,
    classNames: PropTypes.string,
    disabled: PropTypes.bool,
  };

  static defaultProps = {
    size: 'md',
    backgroundColor: E.material.white,
    radii: 2,
    shade: 'light',
  };

  render() {
    let buttonStyle = {
      color: this.props.textColor,
      backgroundColor: this.props.backgroundColor,
      borderRadius: this.props.radii,
    }

    let textColor = {
      color: E.material.grey700
    }
    if (this.props.shade === 'dark') {
      textColor.color = E.material.white;
    }

    let buttonPadding = {
      padding: '.8rem 1.6rem',
      fontSize: '1.4rem',
      lineHeight: (2 / 1.4),
    }

    if (this.props.size === 'xs') {
      buttonPadding = {
        padding: '.6rem 1.2rem',
        fontSize: '1.08rem',
        lineHeight: (1.2 / 1.08),
      }
    } else if (this.props.size === 'sm') {
      buttonPadding = {
        padding: '.6rem 1.2rem',
        fontSize: '1.3rem',
        lineHeight: (2 / 1.3),
      }
    } else if (this.props.size === 'lg') {
      buttonPadding = {
        padding: '1.2rem 2.4rem',
        fontSize: '1.8rem',
        lineHeight: (2.4 / 1.8)
      }
    }

    let buttonClass = classNames(
      'button',
      (this.props.block ? 'button--block' : null),
      (this.props.shade ? 'button--' + this.props.shade : null),
      (this.props.classNames ? this.props.classNames : null)
    );

    let Element = 'button';
    if (this.props.anchor) {
      Element = 'a';
    }

    let disabledStyle = {
      boxShadow: 'none',
      opacity: 0.5
    }

    return (
      <Element
        className={buttonClass}
        style={Object.assign(buttonStyle, textColor, buttonPadding, (this.props.disabled ? disabledStyle : null), this.props.styles)}
        {...this.props}
      >
        <span className='button__layer' style={{borderRadius: this.props.radii}} />
        <span className='button__text'>{this.props.children}</span>
        {(this.props.disabled ? null : <Ink style={{zIndex: 2}} /> )}
      </Element>
    );
  }
}
