import React, { Component, PropTypes } from 'react';
import E from '../constants';

export class Badge extends Component {
  static propTypes = {
    backgroundColor: PropTypes.string,
    shade: PropTypes.oneOf(['light', dark]),
  };

  static defaultProps = {
    backgroundColor: 'rgba(0, 0, 0, .5)',
    shade: 'light'
  };

  render() {
    let labelStyle = {
      backgroundColor: this.props.backgroundColor,
      borderRadius: (1 / 6) + 'em',
      fontSize: '75%',
      textAlign: 'center',
      whiteSpace: 'nowrap',
      padding: '.3em .6em',
      display: 'inline-block',
      lineHeight: 1,
      fontWeight: 500
    }

    let textColor = {
      color: E.material.white,
    }

    return <div className='label' style={Object.assign(labelStyle, this.props.styles)} {...this.props} />;
  }
}
