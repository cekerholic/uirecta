import React, { Component, PropTypes } from 'react';
import E from '../constants';

export class ComponentTemplate extends Component {
  static propTypes = {
    sticky: PropTypes.bool,
    styles: PropTypes.object,
    hamburger: PropTypes.bool,
  };

  static defaultProps = {
    sticky: false,
  };

  render() {
    return <nav className='navbar' {...this.props} />
  }
}
