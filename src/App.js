import React, { Component, PropTypes } from 'react';
import { Col } from '../ui-lib/components/Col';
import { Container } from '../ui-lib/components/Container';
import { Row } from '../ui-lib/components/Row';
import { Wrapper } from '../ui-lib/components/Wrapper';
import { MaterialForm } from '../ui-lib/components/MaterialForm';
import { Button } from '../ui-lib/components/Button';
import { Label } from '../ui-lib/components/Label';
import E from '../ui-lib/constants';

export class App extends Component {
  render() {
    return (
      <Container maxWidth="984">
        <h2>Grid System</h2>
        <Row styles={{marginBottom: 24, textAlign: 'center'}}>
          <Col>
            <div style={{padding: '15px 0', backgroundColor: E.material.lightBlue500, color: E.material.white}}>Full column</div>
          </Col>
        </Row>
        <Row styles={{marginBottom: 24, textAlign: 'center'}}>
          <Col xs="1/2" md="1/3">
            <div style={{padding: '15px 0', backgroundColor: E.material.pink400, color: E.material.white}}>Desktop 33%, Mobile 50%</div>
          </Col>
          <Col xs="1/2" md="1/3">
            <div style={{padding: '15px 0', backgroundColor: E.material.pink600, color: E.material.white}}>Desktop 33%, Mobile 50%</div>
          </Col>
          <Col xs="1/2" md="1/3">
            <div style={{padding: '15px 0', backgroundColor: E.material.pink400, color: E.material.white}}>Desktop 33%, Mobile 50%</div>
          </Col>
        </Row>

        <Row styles={{marginBottom: 24, textAlign: 'center'}}>
          <Col xs="28%">
            <div style={{padding: '15px 0', backgroundColor: E.material.green400, color: E.material.white}}>Width 28%</div>
          </Col>
          <Col xs="72%">
            <div style={{padding: '15px 0', backgroundColor: E.material.green600, color: E.material.white}}>Width 72%</div>
          </Col>
        </Row>

        <Row styles={{marginBottom: 24, textAlign: 'center'}}>
          <Col sm="1/4">
            <div style={{padding: '15px 0', backgroundColor: E.material.purple400, color: E.material.white}}>one quarter</div>
          </Col>
          <Col sm="1/2">
            <div style={{padding: '15px 0', backgroundColor: E.material.purple600, color: E.material.white}}>one half</div>
          </Col>
          <Col sm="1/4">
            <div style={{padding: '15px 0', backgroundColor: E.material.purple400, color: E.material.white}}>one quarter</div>
          </Col>
        </Row>

        <Row styles={{marginBottom: 24, textAlign: 'center'}}>
          <Col pushMd="1/2" sm="1/2">
            <div style={{padding: '15px 0', backgroundColor: E.material.orange600, color: E.material.white}}>Left column, pushed to the right</div>
          </Col>
          <Col pullMd="1/2" sm="1/2">
            <div style={{padding: '15px 0', backgroundColor: E.material.indigo600, color: E.material.white}}>Right column, pulled to the left</div>
          </Col>
          <Col pushMd="1/4" sm="1/2" styles={{marginTop: 24}}>
            <div style={{padding: '15px 0', backgroundColor: E.material.lime600, color: E.material.white}}>One half, offsetted 25%</div>
          </Col>
        </Row>

        <Row styles={{marginTop: 20, marginBottom: 20}}>
          <Col>
            <h2>Material TextField</h2>
          </Col>
          <Col sm="1/3">
            <MaterialForm label="Input Text" id="field0" />
            <br />
            <MaterialForm label="Input Text with hint" id="field3" hint="This is the hint" />
            <br />
            <MaterialForm label="Input Text with custom color" focusColor={E.material.purple500}  hintColor={E.material.purple500} id="field3" hint="This is the hint" />
          </Col>
          <Col sm="1/3">
            <MaterialForm label="Input Text with maxLength (20)" id="field1" maxLength={20} />
            <br />
            <MaterialForm id="field4" disabled value="Disabled with value" />
          </Col>
          <Col sm="1/3">
            <MaterialForm label="Textarea" id="field2" multiline />
          </Col>
        </Row>

        <Row styles={{marginTop: 20, marginBottom: 20}}>
          <Col sm="1/3">
            <h3>Button Sizing</h3>
            <Wrapper styles={{margin: '16px 0'}}><Button size="lg">Large Button</Button></Wrapper>
            <Wrapper styles={{margin: '16px 0'}}><Button>Default Button</Button></Wrapper>
            <Wrapper styles={{margin: '16px 0'}}><Button size="sm">Small Button</Button></Wrapper>
            <Wrapper styles={{margin: '16px 0'}}><Button size="xs">Extra Small Button</Button></Wrapper>
            <Wrapper styles={{margin: '16px 0'}}><Button block>Block Button</Button></Wrapper>
          </Col>
          <Col sm="1/3">
            <h3>Button Color</h3>
            <Wrapper styles={{margin: '16px 0'}}><Button classNames="test">Default Color</Button></Wrapper>
            <Wrapper styles={{margin: '16px 0'}}><Button shade="dark" backgroundColor={E.material.teal500}>Material teal-500</Button></Wrapper>
            <Wrapper styles={{margin: '16px 0'}}><Button shade="dark" backgroundColor={E.material.indigo500}>Material indigo-500</Button></Wrapper>
            <Wrapper styles={{margin: '16px 0'}}><Button shade="dark" backgroundColor={E.material.red500}>Material red-500</Button></Wrapper>
            <Wrapper styles={{margin: '16px 0'}}><Button shade="dark" backgroundColor={E.material.orange500}>Material orange-500</Button></Wrapper>
          </Col>
          <Col sm="1/3">
            <h3>Button Type</h3>
            <Wrapper styles={{margin: '16px 0'}}><Button>{`<button>`} Button</Button></Wrapper>
            <Wrapper styles={{margin: '16px 0'}}><Button anchor href="#">{`<a>`} Button</Button></Wrapper>
            <Wrapper styles={{margin: '16px 0'}}><Button disabled>Disabled Button</Button></Wrapper>
            <Wrapper styles={{margin: '16px 0'}}><Button shade="dark" backgroundColor={E.material.red500} disabled>Disabled Button</Button></Wrapper>
          </Col>
        </Row>

        <Wrapper styles={{marginTop: 20, marginBottom: 20}}>
          <h3>Labels</h3>
          <Label styles={{marginRight: 10}}>This is label</Label>
          <Label styles={{marginRight: 10}} backgroundColor={E.material.deepOrange500}> This is label </Label>
          <Label styles={{marginRight: 10}} backgroundColor={E.material.indigo500}> This is label </Label>
          <Label styles={{marginRight: 10}} backgroundColor={E.material.lightGreen500}> This is label </Label>
          <Label styles={{marginRight: 10}} backgroundColor={E.material.pink500}> This is label </Label>
          <Label styles={{marginRight: 10}} backgroundColor={E.material.amber500}> This is label </Label>
          <Label styles={{marginRight: 10}} backgroundColor={E.material.grey500}> This is label </Label>
        </Wrapper>
      </Container>
    );
  }
}
